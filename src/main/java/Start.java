import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

import w00demo.HomePageOrig;
import w05binding.Persona1Page;
import w05binding.Persona2Page;
import w06monstersofrock.MoRStart;
import w07mercaderias.PedidoPage;

public class Start extends WebPage {

	private static final long serialVersionUID = 7487218035700993833L;

	public Start() {
		super();

		this.configurarPagina();
	}

	private void configurarPagina() {
		this.add(new Link<Void>("app00demo") {
			private static final long serialVersionUID = 1220040936823926824L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePageOrig());
			}
		});

		this.add(new Link<Void>("app01homepage") {
			private static final long serialVersionUID = 5874164763433602530L;

			@Override
			public void onClick() {
				this.setResponsePage(new w01homepage.HomePage());
			}
		});

		this.add(new Link<Void>("app02herencia") {
			private static final long serialVersionUID = -839991422790114168L;

			@Override
			public void onClick() {
				this.setResponsePage(new w02herencia.HijoPage());
			}
		});

		this.add(new Link<Void>("app03superh") {
			private static final long serialVersionUID = 8479816394766559183L;

			@Override
			public void onClick() {
				this.setResponsePage(new w03superherencia.HijoPage());
			}
		});

		this.add(new Link<Void>("app04links") {
			private static final long serialVersionUID = 5340973188582707349L;

			@Override
			public void onClick() {
				this.setResponsePage(new w04links.HomePage());
			}
		});

		this.add(new Link<Void>("app05binding1") {
			private static final long serialVersionUID = 1612368631995375462L;

			@Override
			public void onClick() {
				this.setResponsePage(new Persona1Page());
			}
		});

		this.add(new Link<Void>("app05binding2") {
			private static final long serialVersionUID = -2993252199651312501L;

			@Override
			public void onClick() {
				this.setResponsePage(new Persona2Page());
			}
		});

		this.add(new Link<Void>("app06mor") {
			private static final long serialVersionUID = 3860093336480694377L;

			@Override
			public void onClick() {
				this.setResponsePage(new MoRStart());
			}
		});

		this.add(new Link<Void>("app07merc") {
			private static final long serialVersionUID = 1105621267878952281L;

			@Override
			public void onClick() {
				this.setResponsePage(new PedidoPage());
			}
		});

	}

}
