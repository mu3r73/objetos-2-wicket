package w05binding;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;

@SuppressWarnings("serial")
public class Persona2Page extends WebPage {

	public Persona2Page() {
		super();
		
		Persona pers = new Persona("Sean", "Reinert", "Miami", "seanreinert@aeonspoke");
		CompoundPropertyModel<Persona> cmodel = new CompoundPropertyModel<>(pers);
		super.setDefaultModel(cmodel);
		
		super.add(new Label("nombre"));
		super.add(new Label("apellido"));
		super.add(new Label("direccion"));
		super.add(new Label("email", cmodel.bind("mail")));
		
	}
	
}
