package w05binding;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Persona implements Serializable {
	
	private static final long serialVersionUID = -8566369830745162154L;
	
	private String nombre;
	private String apellido;
	private String direccion;
	private String email;
	private Persona conyuge;
	private Collection<Persona> hijos = new ArrayList<>();
	
	// constructores
	
	public Persona(String nombre, String apellido) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	public Persona(String nombre, String apellido, String direccion, String email) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.email = email;
	}
	
	// getters / setters
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Persona getConyuge() {
		return conyuge;
	}

	public void setConyuge(Persona conyuge) {
		this.conyuge = conyuge;
	}

	public Collection<Persona> getHijos() {
		return hijos;
	}

	public void setHijos(Collection<Persona> hijos) {
		this.hijos = hijos;
	}
	
	public String getMail() {
		return this.getEmail();
	}

	public void setMail(String email) {
		this.setEmail(email);
	}
	
}
