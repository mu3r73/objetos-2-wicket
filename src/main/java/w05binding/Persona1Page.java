package w05binding;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;

@SuppressWarnings("serial")
public class Persona1Page extends WebPage {
	
	public Persona1Page() {
		super();
		
		Persona pers = new Persona("Vinnie", "Paul");
		
		super.add(new Label("lblnombre", new PropertyModel<>(pers, "nombre")));
		super.add(new Label("lblapellido", new PropertyModel<>(pers, "apellido")));
	}
	
}
