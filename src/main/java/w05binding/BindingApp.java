package w05binding;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

public class BindingApp extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
//		return Persona1Page.class;
		return Persona2Page.class;
	}

}
