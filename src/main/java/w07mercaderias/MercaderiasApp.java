package w07mercaderias;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

public class MercaderiasApp extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return PedidoPage.class;
	}

//	@Override
//	public RuntimeConfigurationType getConfigurationType() {
//		return RuntimeConfigurationType.DEPLOYMENT;
//	}
	
}
