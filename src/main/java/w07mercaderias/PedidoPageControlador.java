package w07mercaderias;

import java.io.Serializable;

import p06_mercaderias.modelo.Pedido;
import p06_mercaderias.modelo.Renglon;

public class PedidoPageControlador implements Serializable {
	
	private static final long serialVersionUID = -6549759523797783122L;
	
	private Pedido pedido;
	private Renglon renglon;
	
	// getters / setters
	
	public Pedido getPedido() {
		return this.pedido;
	}
	
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public Renglon getRenglon() {
		return this.renglon;
	}
	
	public void setRenglon(Renglon renglon) {
		this.renglon = renglon;
	}

	public boolean hayRenglonElegido() {
		return this.getRenglon() != null;
	}
	
}
