package w07mercaderias;

import java.io.Serializable;

import p06_mercaderias.modelo.Pedido;
import p06_mercaderias.modelo.Renglon;

public class RenglonControlador implements Serializable {
	
	private static final long serialVersionUID = 860476274686584343L;
	
	private Pedido pedido;
	private Renglon renglon;
	
	public RenglonControlador() {
		super();
		
		this.renglon = new Renglon();
	}
	
	// getters / setters
	
	public Pedido getPedido() {
		return this.pedido;
	}
	
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public Renglon getRenglon() {
		return this.renglon;
	}
	
	public void setRenglon(Renglon renglon) {
		this.renglon = renglon;
	}
	
	// acciones
	
	public void agregarRenglon() {
		this.pedido.agregarRenglon(this.renglon);
	}
	
}
