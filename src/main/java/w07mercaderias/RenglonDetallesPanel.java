package w07mercaderias;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

public class RenglonDetallesPanel extends Panel {
	
	private static final long serialVersionUID = -813745166272793668L;
	
	private PedidoPageControlador ctrl;
	
	public RenglonDetallesPanel(String id, PedidoPageControlador ctrl) {
		super(id);
		this.ctrl = ctrl;
		
		this.configurarPanel();
	}

	private void configurarPanel() {
		this.add(new Label("producto", new PropertyModel<>(ctrl, "renglon.producto.nombre")));
		this.add(new Label("cantidad", new PropertyModel<>(ctrl, "renglon.cantidad")));
		this.add(new Label("costoproducto", new PropertyModel<>(ctrl, "renglon.producto.costo")));
		this.add(new Label("costorenglon", new PropertyModel<>(ctrl, "renglon.costo")));
		
		Link<String> btnBorrar = new Link<String>("borrar") {
			private static final long serialVersionUID = 6375414592375248897L;

			@Override
			public void onClick() {
				RenglonDetallesPanel.this.ctrl.getPedido().borrarRenglon(RenglonDetallesPanel.this.ctrl.getRenglon());
				this.setResponsePage(new PedidoPage(RenglonDetallesPanel.this.ctrl.getPedido()));
			}
		};
		this.add(btnBorrar);
		
		this.setVisible(false);
	}
	
}
