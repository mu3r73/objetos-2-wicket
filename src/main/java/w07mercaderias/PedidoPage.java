package w07mercaderias;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import p06_mercaderias.stores.PedidoAppStore;
import p06_mercaderias.modelo.Pedido;
import p06_mercaderias.modelo.Renglon;

public class PedidoPage extends WebPage {
	
	private static final long serialVersionUID = -4246354708951235063L;
	
	private PedidoAppStore ps;
	private PedidoPageControlador ctrl;
	private Panel pnlRenglon;
	
	public PedidoPage() {
		super();
		
		this.initStore();
		
		this.ctrl = new PedidoPageControlador();
		this.ctrl.setPedido(this.ps.getPedidoEjemplo());
		
		this.configurarPagina();
	}
	
	public PedidoPage(Pedido pedido) {
		super();
		
		this.initStore();
		
		this.ctrl = new PedidoPageControlador();
		this.ctrl.setPedido(pedido);
		
		this.configurarPagina();
	}

	private void configurarPagina() {
		this.pnlRenglon = new RenglonDetallesPanel("renglondetalles", this.ctrl);
		this.refreshPanelRenglon();
		
		this.add(new ListView<Renglon>("renglones", new PropertyModel<>(this.ctrl, "pedido.renglones")) {
			private static final long serialVersionUID = -1040484248651953489L;
			
			@Override
			protected void populateItem(ListItem<Renglon> item) {
				Renglon renglon = item.getModelObject();
				
				Link<String> lnkProducto = new Link<String>("producto") {
					private static final long serialVersionUID = -89469309014926356L;
					
					@Override
					public void onClick() {
						PedidoPage.this.ctrl.setRenglon(renglon);
						PedidoPage.this.refreshPanelRenglon();
					}
				};
				lnkProducto.setBody(new PropertyModel<>(renglon, "producto.nombre"));
				item.add(lnkProducto);
				
//				item.add(new Label("producto", new PropertyModel<>(renglon, "producto.nombre")));
				item.add(new Label("cantidad", new PropertyModel<>(renglon, "cantidad")));
				item.add(new Label("costo", new PropertyModel<>(renglon, "costo")));
			}
		});
		this.add(new Label("total", new PropertyModel<>(this.ctrl, "pedido.costo")));
		
		Link<String> btnAgregar = new Link<String>("agregar") {
			private static final long serialVersionUID = 8001338122014874709L;

			@Override
			public void onClick() {
				this.setResponsePage(new RenglonForm(PedidoPage.this.ctrl.getPedido()));
			}
		};
		this.add(btnAgregar);
		
		this.add(this.pnlRenglon);
	}
	
	protected void refreshPanelRenglon() {
		this.pnlRenglon.setVisible(this.ctrl.hayRenglonElegido());
	}
	
	private void initStore() {
		this.ps = PedidoAppStore.getInstancia();
		this.ps.cargarProductosEjemplo();
	}
	
}
