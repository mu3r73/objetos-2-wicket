package w07mercaderias;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import p06_mercaderias.modelo.Pedido;
import p06_mercaderias.modelo.Renglon;
import p06_mercaderias.stores.PedidoAppStore;

public class RenglonForm extends WebPage {
	
	private static final long serialVersionUID = -3284087218849605619L;
	
	private PedidoAppStore ps;
	private RenglonControlador ctrl;
	
	// constructores
	
	public RenglonForm(Pedido pedido) {
		super();
		
		this.initStore();
		
		this.ctrl = new RenglonControlador();
		this.ctrl.setPedido(pedido);
		
		this.add(this.buildForm());
	}
	
	private Form<Renglon> buildForm() {
		Form<Renglon> fr = new Form<Renglon>("formrenglon") {
			private static final long serialVersionUID = 8889169085663055545L;
			
			@Override
			protected void onSubmit() {
				RenglonForm.this.ctrl.agregarRenglon();
				this.setResponsePage(new PedidoPage(RenglonForm.this.ctrl.getPedido()));
			}
		};
		
		fr.add(new DropDownChoice<>("producto", new PropertyModel<>(this.ctrl, "renglon.producto"),
												new PropertyModel<>(this.ps, "productosOrdenNombre"),
												new ChoiceRenderer<>("nombre")));
		
		fr.add(new TextField<>("cantidad", new PropertyModel<>(this.ctrl, "renglon.cantidad")));
		
//		fr.add(new Label("costorenglon", new PropertyModel<>(this.ctrl, "renglon.costo")));
		
		return fr;
	}
	
	private void initStore() {
		ps = PedidoAppStore.getInstancia();
		ps.cargarProductosEjemplo();
	}
	
}
