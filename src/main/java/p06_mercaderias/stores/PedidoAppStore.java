package p06_mercaderias.stores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import p06_mercaderias.modelo.Pedido;
import p06_mercaderias.modelo.Producto;
import p06_mercaderias.modelo.ProductoComprado;

// singleton
public class PedidoAppStore implements Serializable {

	private static final long serialVersionUID = 5416057384916598298L;
	
	private Collection<Producto> productos;
	private static PedidoAppStore instancia;
	
	// constructores
	
	private PedidoAppStore() {
		super();
	}

	// instancia singleton
	
	public static PedidoAppStore getInstancia() {
		if (instancia == null) {
			instancia = new PedidoAppStore();
		}
		return instancia;
	}
	
	// productos truchos
	
	public void cargarProductosEjemplo() {
		this.productos = new ArrayList<>();
		this.productos.add(new ProductoComprado("aceite", 0.9, 4, 30));
		this.productos.add(new ProductoComprado("arroz", 0.5, 4, 15));
		this.productos.add(new ProductoComprado("arvejas en lata", 0.3, 3, 10));
		this.productos.add(new ProductoComprado("atún", 0.2, 3, 40));
		this.productos.add(new ProductoComprado("azúcar", 1, 5, 15));
		this.productos.add(new ProductoComprado("caballa", 0.4, 3, 35));
		this.productos.add(new ProductoComprado("cacao", 0.35, 4, 20));
		this.productos.add(new ProductoComprado("café", 0.1, 4, 45));
		this.productos.add(new ProductoComprado("choclo en lata", 0.3, 3, 15));
		this.productos.add(new ProductoComprado("harina", 1, 5, 10));
		this.productos.add(new ProductoComprado("lentejas", 0.2, 3, 15));
		this.productos.add(new ProductoComprado("mix de cereales", 0.1, 4, 20));
		this.productos.add(new ProductoComprado("pan", 0.2, 6, 30));
		this.productos.add(new ProductoComprado("polenta", 0.4, 5, 10));
		this.productos.add(new ProductoComprado("porotos en lata", 0.35, 3, 15));
		this.productos.add(new ProductoComprado("sal fina", 0.5, 6, 60));
		this.productos.add(new ProductoComprado("té", 0.5, 5, 15));
		this.productos.add(new ProductoComprado("vinagre", 0.75, 4, 15));
		this.productos.add(new ProductoComprado("yerba", 1, 5, 50));
	}
	
	// consultas
	
	public Producto getProducto(String nombre) {
		return this.productos.stream()
				.filter(producto -> producto.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
	public List<Producto> getProductosOrdenNombre() {
		return this.productos.stream()
				.sorted(Comparator.comparing(Producto::getNombre))
				.collect(Collectors.toList());
	}

	public Pedido getPedidoEjemplo() {
		Pedido p = new Pedido();
		
		p.agregarRenglon(3, this.getProducto("arroz"));
		p.agregarRenglon(1, this.getProducto("café"));
		p.agregarRenglon(2, this.getProducto("pan"));
		p.agregarRenglon(2, this.getProducto("yerba"));
		
		return p;
	}
	
}
