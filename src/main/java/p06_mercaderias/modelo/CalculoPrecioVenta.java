package p06_mercaderias.modelo;

public interface CalculoPrecioVenta {
	// patrón strategy

	double getPrecioVenta(Producto producto);

}
