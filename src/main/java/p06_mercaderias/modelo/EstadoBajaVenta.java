package p06_mercaderias.modelo;

public class EstadoBajaVenta implements EstadoEstacional {

	@Override
	public double getPrecioVenta(Producto producto) {
		return PrecioVentaLiquidacion.getInstancia()
				.getPrecioVenta(producto);
	}

}
