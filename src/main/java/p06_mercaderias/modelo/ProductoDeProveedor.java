package p06_mercaderias.modelo;

import java.io.Serializable;

public abstract class ProductoDeProveedor extends Producto implements Serializable {
	
	private static final long serialVersionUID = 1988186320195286519L;
	
	public double precioCompra;

	// constructores
	
	public ProductoDeProveedor(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra) {
		super(nombre, pesoProm, valorAlmacPorKg);
		this.precioCompra = precioCompra;
	}
	
	// getters / setters
	
	public double getPrecioCompra() {
		return this.precioCompra;
	}
	
	// consultas
	
	@Override
	public double getCostoProduccion() {
		return this.precioCompra;
	}
	
}
