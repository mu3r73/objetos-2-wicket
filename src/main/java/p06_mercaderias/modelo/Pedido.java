package p06_mercaderias.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 */
public class Pedido implements Serializable {

	private static final long serialVersionUID = -3041325156895003865L;
	
	private List<Renglon> renglones = new ArrayList<>(); 
	
	// getters y setters
	
	public List<Renglon> getRenglones() {
		return this.renglones;
	}
	
	public void setRenglones(List<Renglon> renglones) {
		this.renglones = renglones;
	}
	
	// altas a colecciones
	
	public void agregarRenglon(Renglon renglon) {
		this.renglones.add(renglon);
	}
	
	public void agregarRenglon(int cantidad, Producto producto) {
		this.renglones.add(new Renglon(cantidad, producto));
	}
	
	// bajas de colecciones
	
	public void borrarRenglon(Renglon renglon) {
		this.renglones.remove(renglon);
	}
	
	// consultas
	
	/**
	 * punto 1.1: costo de un pedido 
	 */
	public double getCosto() {
		return renglones.stream()
				.mapToDouble(renglon -> renglon.getCosto())
				.sum();
	}
	
	/**
	 * punto 1.2: lista de productos delicados, ordenada por nombre del producto
	 */
	public List<Producto> getProductosDelicados() {
		return this.getProductos().stream()
				.filter(producto -> producto.esDelicado())
				.sorted(Comparator.comparing(Producto::getNombre))
				.collect(Collectors.toList());
	}
	
	/**
	 * punto 1.3: lista de productos en el pedido
	 */
	public List<Producto> getProductos() {
		return this.renglones.stream()
				.map(renglon -> renglon.getProducto())
				.collect(Collectors.toList());
	}

}
