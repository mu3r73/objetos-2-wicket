package p06_mercaderias.modelo;

public interface EstadoEstacional {
	// patrón State simplificado (sin clase contexto)

	double getPrecioVenta(Producto producto);

}
