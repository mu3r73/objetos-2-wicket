package p06_mercaderias.modelo;

import java.io.Serializable;

public class ProductoComprado extends ProductoDeProveedor implements Serializable {
	
	private static final long serialVersionUID = -7531689155415531526L;
	
	private static final double plusPorTrasladoAlDeposito = 0.2; // %
	
	// constructores
	
	public ProductoComprado(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra) {
		super(nombre, pesoProm, valorAlmacPorKg, precioCompra);
	}
	
	// consultas
	
	@Override
	public double getCostoAlmacenamiento() {
		return super.getCostoAlmacenamiento() * (1 + ProductoComprado.plusPorTrasladoAlDeposito);
	}
	
}
