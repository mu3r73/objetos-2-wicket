package w06monstersofrock.e02.bandaventasporpais;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.CopiasPorPais;
import p05_monsters_of_rock.modelo.Pais;
import w06monstersofrock.MoRStart;

public class BandaCVPorPaisTabPage extends WebPage {

	private static final long serialVersionUID = 5463797189425405311L;

	private BandaCVPorPaisTabControlador ctrl;
	private Panel pnlVentas;

	public BandaCVPorPaisTabPage() {
		super();

		this.ctrl = new BandaCVPorPaisTabControlador();

		this.configurarPagina();
	}

	private void configurarPagina() {
		this.add(new Link<Object>("mor") {
			private static final long serialVersionUID = -2599661747556940904L;

			@Override
			public void onClick() {
				super.setResponsePage(MoRStart.class);
			}
		});
		
		this.add(new ListView<Pais>("thead", new PropertyModel<>(this.ctrl, "paisesOrdenNombre")) {
			private static final long serialVersionUID = 8507902435685662533L;

			@Override
			protected void populateItem(ListItem<Pais> listItem) {
				Pais p = listItem.getModelObject();
				listItem.add(new Label("bandacvxpaisth", new PropertyModel<String>(p, "nombre")));
			}
		});
		
		this.pnlVentas = new VentasDetallesPanel("ventasdetalles", this.ctrl);
		this.refreshPanelVentas();

		this.add(new ListView<BandaVentasPorPais>("trow", new PropertyModel<>(this.ctrl, "bandasVentasPorPais")) {
			private static final long serialVersionUID = -1420639612393569898L;

			@Override
			protected void populateItem(ListItem<BandaVentasPorPais> litemBvpp) {
				BandaVentasPorPais bvpp = litemBvpp.getModelObject();
				
				litemBvpp.add(new Label("banda", new PropertyModel<>(bvpp, "banda.nombre")));

				litemBvpp.add(new ListView<CopiasPorPais>("bandacvxpaistd",
						new PropertyModel<>(bvpp, "ventasPorPais")) {
					private static final long serialVersionUID = 5921133586286400881L;

					@Override
					protected void populateItem(ListItem<CopiasPorPais> litemCpp) {
						CopiasPorPais cpp = litemCpp.getModelObject();
						
//						litemCpp.add(new Label("ventas", new PropertyModel<>(cpp, "copias")));
						Link<String> lnkVentas = new Link<String>("ventas") {
							private static final long serialVersionUID = 8671250942392096712L;

							@Override
							public void onClick() {
								BandaCVPorPaisTabPage.this.ctrl.setBanda(bvpp.getBanda());
								BandaCVPorPaisTabPage.this.ctrl.setPais(cpp.getPais());
								BandaCVPorPaisTabPage.this.refreshPanelVentas();
							}
						};
						lnkVentas.setBody(new PropertyModel<>(cpp, "copias"));
						litemCpp.add(lnkVentas);
					}
				});
			}
		});
		
		this.add(this.pnlVentas);
	}

	protected void refreshPanelVentas() {
		this.pnlVentas.setVisible(this.ctrl.hayBandaYPaisElegidos());
	}

}
