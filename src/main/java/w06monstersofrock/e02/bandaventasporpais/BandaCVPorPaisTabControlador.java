package w06monstersofrock.e02.bandaventasporpais;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.CopiasPorPais;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.stores.BandasStore;
import p05_monsters_of_rock.stores.DiscosStore;
import p05_monsters_of_rock.stores.PaisesStore;

public class BandaCVPorPaisTabControlador implements Serializable {

	private static final long serialVersionUID = 1255021828314087775L;
	
	private BandasStore bs;
	private PaisesStore ps;
	private DiscosStore ds;
	
	private Banda banda;
	private Pais pais;
	
	// constructores
	
	public BandaCVPorPaisTabControlador() {
		super();

		this.initStores();
	}
	
	// getters / setters
	
	public Banda getBanda() {
		return this.banda;
	}

	public void setBanda(Banda banda) {
		this.banda = banda;
	}

	public Pais getPais() {
		return this.pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	// consultas
	
	public Collection<Pais> getPaisesOrdenNombre() {
		return this.ps.getPaisesOrdenNombre();
	}

	public Collection<Banda> getBandasOrdenNombre() {
		return this.bs.getBandasOrdenNombre();
	}

	public Collection<BandaVentasPorPais> getBandasVentasPorPais() {
		Collection<BandaVentasPorPais> bvpp =  new ArrayList<>();
		for (Banda b : this.getBandasOrdenNombre()) {
			Collection<CopiasPorPais> ccpp = new ArrayList<>();
			for (Pais p : this.getPaisesOrdenNombre()) {
				ccpp.add(new CopiasPorPais(p, b.cuantasCopiasVendioEn(p)));
			}
			bvpp.add(new BandaVentasPorPais(b, ccpp));
		}
		return bvpp;
	}
	
	public Collection<CopiasPorDisco> getCopiasPorDisco() {
		List<CopiasPorDisco> cpd = new ArrayList<>();
		if (this.hayBandaYPaisElegidos()) {
			for (Disco d : this.banda.getDiscosOrdenAnio()) {
				cpd.add(new CopiasPorDisco(d, d.copiasVendidasEn(this.pais)));
			}
		}
		return cpd;
	}
	
	public boolean hayBandaYPaisElegidos() {
		return (this.banda != null) && (this.pais != null);
	}
	
	// acciones
	
	private void initStores() {
		this.bs = BandasStore.getInstancia();
		this.bs.cargarDatosPrueba();

		this.ps = PaisesStore.getInstancia();
		this.ps.cargarDatosPrueba();

		this.ds = DiscosStore.getInstancia();
		this.ds.cargarDatosPrueba();
	}

}
