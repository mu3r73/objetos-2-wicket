package w06monstersofrock.e02.bandaventasporpais;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

public class VentasDetallesPanel extends Panel {

	private static final long serialVersionUID = 6296407674301587353L;

	private BandaCVPorPaisTabControlador ctrl;
	
	public VentasDetallesPanel(String id, BandaCVPorPaisTabControlador ctrl) {
		super(id);
		this.ctrl = ctrl;
		
		this.configurarPanel();
	}
	
	private void configurarPanel() {
		this.add(new Label("banda", new PropertyModel<>(this.ctrl, "banda.nombre")));
		this.add(new Label("pais", new PropertyModel<>(this.ctrl, "pais.nombre")));
		
		ListView<CopiasPorDisco> lvcpd =
				new ListView<CopiasPorDisco>("copiaspordisco",
											new PropertyModel<>(this.ctrl, "copiasPorDisco")) {
					private static final long serialVersionUID = -9162616383767632947L;

					@Override
					protected void populateItem(ListItem<CopiasPorDisco> item) {
						CopiasPorDisco cpd = item.getModelObject();
						item.add(new Label("disco", cpd.getDisco().getNombre()));
						item.add(new Label("copias", cpd.getCopias()));
					}
		};
		this.add(lvcpd);
		
		this.setVisible(false);
	}
	
}
