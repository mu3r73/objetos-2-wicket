package w06monstersofrock.e02.bandaventasporpais;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.CopiasPorPais;

public class BandaVentasPorPais implements Serializable {
	
	private static final long serialVersionUID = 3035352420986644756L;
	
	private Banda banda;
	private Collection<CopiasPorPais> vpp;
	
	// constructores
	
	public BandaVentasPorPais() {
		super();
		
		vpp = new ArrayList<>();
	}

	public BandaVentasPorPais(Banda banda, Collection<CopiasPorPais> vpp) {
		super();
		
		this.banda = banda;
		this.vpp = vpp;
	}

	// getters / setters
	
	public Banda getBanda() {
		return this.banda;
	}

	public Collection<CopiasPorPais> getVentasPorPais() {
		return this.vpp;
	}

}
