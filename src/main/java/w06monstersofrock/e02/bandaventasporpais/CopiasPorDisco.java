package w06monstersofrock.e02.bandaventasporpais;

import p05_monsters_of_rock.modelo.Disco;

public class CopiasPorDisco {
	
	private Disco disco;
	private int copias;
	
	// construtores
	
	public CopiasPorDisco() {
		super();
	}

	public CopiasPorDisco(Disco disco, int copias) {
		super();
		this.disco = disco;
		this.copias = copias;
	}

	// getters / setters
	
	public Disco getDisco() {
		return this.disco;
	}
	
	public void setDisco(Disco disco) {
		this.disco = disco;
	}
	
	public int getCopias() {
		return this.copias;
	}
	
	public void setCopias(int copias) {
		this.copias = copias;
	}
		
}
