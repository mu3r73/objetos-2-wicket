package w06monstersofrock;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import w06monstersofrock.e01.bandas.BandaPage;

public class MonstersOfRockApp extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return BandaPage.class;
	}
	
//	@Override
//	public RuntimeConfigurationType getConfigurationType() {
//		return RuntimeConfigurationType.DEPLOYMENT;
//	}

}
