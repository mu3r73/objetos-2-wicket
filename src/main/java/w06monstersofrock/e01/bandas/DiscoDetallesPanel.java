package w06monstersofrock.e01.bandas;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.CopiasPorPais;

public class DiscoDetallesPanel extends Panel {
	
	private static final long serialVersionUID = -2495527282899461069L;
	
	private BandaPageControlador ctrl;
	
	public DiscoDetallesPanel(String id, BandaPageControlador ctrl) {
		super(id);
		this.ctrl = ctrl;
		
		this.configurarPanel();
	}
	
	private void configurarPanel() {
		this.add(new Label("nombre", new PropertyModel<>(this.ctrl, "nombreDisco")));
		
		ListView<CopiasPorPais> lvcpp =
				new ListView<CopiasPorPais>("copiasporpais",
											new PropertyModel<>(this.ctrl, "disco.copiasPorPais")) {
					private static final long serialVersionUID = 2067507571827298676L;
					
					@Override
					protected void populateItem(ListItem<CopiasPorPais> item) {
						CopiasPorPais cpp = item.getModelObject();
						item.add(new Label("pais", cpp.getPais().getNombre()));
						item.add(new Label("copias", cpp.getCopias()));
					}
		};
		this.add(lvcpp);
		
		Link<String> btnAgregarCopias = new Link<String>("agregarcopias") {
			private static final long serialVersionUID = 1336119822724112897L;

			@Override
			public void onClick() {
				this.setResponsePage(new CopiasPorPaisForm(DiscoDetallesPanel.this.ctrl.getBanda(),
															DiscoDetallesPanel.this.ctrl.getDisco()));
			}
		};
		this.add(btnAgregarCopias);
		
		this.setVisible(false);
	}
	
}
