package w06monstersofrock.e01.bandas;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.CopiasPorPais;
import p05_monsters_of_rock.modelo.Disco;

public class CopiasPorPaisForm extends WebPage {
	
	private static final long serialVersionUID = -7438338608699374727L;
	
	private CopiasPorPaisControlador ctrl;
	
	public CopiasPorPaisForm(Banda banda, Disco disco) {
		super();
		
		this.ctrl = new CopiasPorPaisControlador();
		this.ctrl.setBanda(banda);
		this.ctrl.setDisco(disco);
		
		this.add(new Label("disco", new PropertyModel<>(this.ctrl, "disco.nombre")));
		this.add(new Label("banda", new PropertyModel<>(this.ctrl, "banda.nombre")));
		
		this.add(this.buildForm());
	}

	private Form<CopiasPorPais> buildForm() {
		Form<CopiasPorPais> fcpp = new Form<CopiasPorPais>("formcopiasporpais") {
			private static final long serialVersionUID = -6900047799525679769L;
			
			@Override
			protected void onSubmit() {
				CopiasPorPaisForm.this.ctrl.agregarCopias();
				this.setResponsePage(new BandaPage(CopiasPorPaisForm.this.ctrl.getBanda(),
													CopiasPorPaisForm.this.ctrl.getDisco()));
			}
			
		};
		
		fcpp.add(new DropDownChoice<>("pais", new PropertyModel<>(this.ctrl, "pais"),
												new PropertyModel<>(this.ctrl, "paises"),
												new ChoiceRenderer<>("nombre")));
//		fcpp.add(new TextField<>("pais", new PropertyModel<>(this.ctrl, "pais")));
		
		fcpp.add(new TextField<>("copias", new PropertyModel<>(this.ctrl, "copias")));
		
		return fcpp;
	}
	
}
