package w06monstersofrock.e01.bandas;

import java.io.Serializable;
import java.util.Collection;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.stores.PaisesStore;

public class CopiasPorPaisControlador implements Serializable {
	
	private static final long serialVersionUID = -7368889080434989476L;
	
	private PaisesStore ps;
	private Banda banda;
	private Disco disco;
	private Pais pais;
	private int copias = 0;
	
	public CopiasPorPaisControlador() {
		super();
		this.initStores();
	}

	// getters / setters
	
	public Banda getBanda() {
		return this.banda;
	}
	
	public void setBanda(Banda banda) {
		this.banda = banda;
	}
	
	public Disco getDisco() {
		return this.disco;
	}
	
	public void setDisco(Disco disco) {
		this.disco = disco;
	}
	
	public Pais getPais() {
		return this.pais;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public int getCopias() {
		return this.copias;
	}
	
	public void setCopias(int copias) {
		this.copias = copias;
	}
	
	// acciones
	
	public void agregarCopias() {
		this.getDisco().agregarCopiasVendidas(this.getPais(), this.getCopias());
	}
	
	private void initStores() {
		this.ps = PaisesStore.getInstancia();
		this.ps.cargarDatosPrueba();
	}
	
	// consultas
	
	public Collection<Pais> getPaises() {
		return this.ps.getPaisesOrdenNombre(); 
	}
	
}
