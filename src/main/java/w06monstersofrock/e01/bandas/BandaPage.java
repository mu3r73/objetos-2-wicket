package w06monstersofrock.e01.bandas;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;
import w06monstersofrock.MoRStart;

public class BandaPage extends WebPage {
	
	private static final long serialVersionUID = -2552845899752471533L;
	
	private BandaPageControlador ctrl;
	private Panel pnlDisco;
	
	public BandaPage() {
		super();
		
		this.ctrl = new BandaPageControlador();
		this.ctrl.setBanda(this.ctrl.getBandaPredeterminada());
		
		this.configurarPagina();
	}
	
	public BandaPage(Banda banda) {
		super();
		
		this.ctrl = new BandaPageControlador();
		this.ctrl.setBanda(banda);
		
		this.configurarPagina();
	}

	public BandaPage(Banda banda, Disco disco) {
		super();
		
		this.ctrl = new BandaPageControlador();
		this.ctrl.setBanda(banda);
		this.ctrl.setDisco(disco);
		
		this.configurarPagina();
	}

	private void configurarPagina() {
		this.add(new Link<Object>("mor") {
			private static final long serialVersionUID = -6737544487513890469L;

			@Override
			public void onClick() {
				super.setResponsePage(MoRStart.class);
			}
		});
		
		this.add(new Label("nombre", new PropertyModel<>(this.ctrl, "banda.nombre")));
		this.add(new Label("pais", new PropertyModel<>(this.ctrl, "banda.pais.nombre")));
		this.add(new Label("genero", new PropertyModel<>(this.ctrl, "banda.genero")));
		this.add(new Label("copiasvendidas", new PropertyModel<>(this.ctrl, "banda.totalCopiasVendidas")));
		
		PropertyModel<Banda> pmbanda = new PropertyModel<>(this.ctrl, "banda");
		
		Form<Banda> fcpp = new Form<Banda>("formbanda") {
			private static final long serialVersionUID = 7909558294030995861L;

			@Override
			protected void onSubmit() {
				BandaPage.this.ctrl.setBanda(pmbanda.getObject());
				this.setResponsePage(new BandaPage(BandaPage.this.ctrl.getBanda()));
			}
		};
		
		fcpp.add(new DropDownChoice<>("banda", pmbanda,
				new PropertyModel<>(this.ctrl, "bandas"),
				new ChoiceRenderer<>("nombre")));
		this.add(fcpp);
		
		this.pnlDisco = new DiscoDetallesPanel("discodetalles", this.ctrl);
		this.refreshPanelDisco();
		
		this.add(new ListView<Disco>("discos", new PropertyModel<>(this.ctrl, "banda.discos")) {
			private static final long serialVersionUID = 6961705720493012582L;

			@Override
			protected void populateItem(ListItem<Disco> item) {
				Disco disco = item.getModelObject();
				
				Link<String> lnkDiscoNombre = new Link<String>("nombre") {
					private static final long serialVersionUID = -9209760365684274100L;

					@Override
					public void onClick() {
						BandaPage.this.ctrl.setDisco(disco);
						BandaPage.this.refreshPanelDisco();
					}
				};
				lnkDiscoNombre.setBody(new PropertyModel<>(disco, "nombre"));
				item.add(lnkDiscoNombre);
//				item.add(new Label("nombre", new PropertyModel<>(disco, "nombre")));
				
				item.add(new Label("anioDeEdicion", new PropertyModel<>(disco, "anioDeEdicion")));
				item.add(new Label("totalCopiasVendidas", new PropertyModel<>(disco, "totalCopiasVendidas")));
			}
		});
		
		this.add(this.pnlDisco);
	}
	
	private void refreshPanelDisco() {
		this.pnlDisco.setVisible(this.ctrl.hayDiscoElegido());
	}
		
}
