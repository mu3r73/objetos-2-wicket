package w06monstersofrock.e01.bandas;

import java.io.Serializable;
import java.util.Collection;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.stores.BandasStore;
import p05_monsters_of_rock.stores.DiscosStore;

public class BandaPageControlador implements Serializable {
	
	private static final long serialVersionUID = -6405224851264084454L;
	
	private BandasStore bs;
	private DiscosStore ds;
	private Banda banda;
	private Disco disco;
	
	public BandaPageControlador() {
		super();
		this.initStores();
	}

	// getters / setters
	
	public Banda getBanda() {
		return this.banda;
	}
	
	public void setBanda(Banda banda) {
		this.banda = banda;
	}

	public Disco getDisco() {
		return this.disco;
	}

	public void setDisco(Disco disco) {
		this.disco = disco;
	}
	
	public boolean hayDiscoElegido() {
		return this.getDisco() != null;
	}
	
	public String nombreDisco() {
		return this.getDisco().getNombre();
	}

	public Banda getBandaPredeterminada() {
		return this.bs.getBanda("Gorguts");
	}
	
	private void initStores() {
		this.bs = BandasStore.getInstancia();
		this.bs.cargarDatosPrueba();
		
		this.ds = DiscosStore.getInstancia();
		this.ds.cargarDatosPrueba();
	}
	
	public Collection<Banda> getBandas() {
		return bs.getBandasOrdenNombre();
	}
	
}
