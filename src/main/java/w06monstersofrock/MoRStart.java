package w06monstersofrock;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

import w06monstersofrock.e01.bandas.BandaPage;
import w06monstersofrock.e02.bandaventasporpais.BandaCVPorPaisTabPage;
import w06monstersofrock.e03.ciudades.CiudadPage;
import w06monstersofrock.e04.crud.BandasPage;

public class MoRStart extends WebPage {

	private static final long serialVersionUID = 7487218035700993833L;

	public MoRStart() {
		super();

		this.configurarPagina();
	}

	private void configurarPagina() {
		this.add(new Link<Void>("app01banda") {
			private static final long serialVersionUID = 6810759216226432601L;

			@Override
			public void onClick() {
				this.setResponsePage(BandaPage.class);
			}
		});

		this.add(new Link<Void>("app02ventas") {
			private static final long serialVersionUID = -7403542441434177504L;

			@Override
			public void onClick() {
				this.setResponsePage(BandaCVPorPaisTabPage.class);
			}			
		});

		this.add(new Link<Void>("app03ciudad") {
			private static final long serialVersionUID = 9223047576728891699L;

			@Override
			public void onClick() {
				this.setResponsePage(CiudadPage.class);
			}
		});

		this.add(new Link<Void>("app04crud") {
			private static final long serialVersionUID = -1632122359572246584L;

			@Override
			public void onClick() {
				this.setResponsePage(BandasPage.class);
			}
		});

	}

}
