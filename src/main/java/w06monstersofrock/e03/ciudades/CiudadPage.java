package w06monstersofrock.e03.ciudades;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.Ciudad;
import p05_monsters_of_rock.modelo.Pais;
import w06monstersofrock.MoRStart;

public class CiudadPage extends WebPage {

	private static final long serialVersionUID = 3041078026225967327L;
	
	private CiudadPageControlador ctrl;
	
	public CiudadPage() {
		super();
		
		this.ctrl = new CiudadPageControlador();
		this.ctrl.setPais(null);
		
		this.configurarPanel();
	}

	public CiudadPage(Pais pais) {
		super();
		
		this.ctrl = new CiudadPageControlador();
		this.ctrl.setPais(pais);
		
		this.configurarPanel();
	}

	private void configurarPanel() {
		this.add(new Link<Object>("mor") {
			private static final long serialVersionUID = -8303168634756090480L;

			@Override
			public void onClick() {
				super.setResponsePage(MoRStart.class);
			}
		});
		
		ListView<Ciudad> lvc =
				new ListView<Ciudad>("ciudades", new PropertyModel<>(this.ctrl, "ciudadesOrdenNombre")) {
					private static final long serialVersionUID = 4761167671259293474L;

					@Override
					protected void populateItem(ListItem<Ciudad> item) {
						Ciudad c = item.getModelObject();
						item.add(new Label("ciudad", c.getNombre()));
						
//						item.add(new Label("pais", c.getPais().getNombre()));
						Link<String> lnkPaisNombre = new Link<String>("pais") {
							private static final long serialVersionUID = -4644132733711252274L;

							@Override
							public void onClick() {
								CiudadPage.this.ctrl.setPais(c.getPais());
								this.setResponsePage(new CiudadPage(CiudadPage.this.ctrl.getPais()));
							}
						};
						lnkPaisNombre.setBody(new PropertyModel<>(c.getPais(), "nombre"));
						item.add(lnkPaisNombre);

					}
		};
		this.add(lvc);
		
		Link<String> btnVerTodas = new Link<String>("vertodas") {
			private static final long serialVersionUID = 574567564893775485L;

			@Override
			public void onClick() {
				CiudadPage.this.ctrl.setPais(null);
				this.setResponsePage(CiudadPage.class);
			}
		};
		this.add(btnVerTodas);

		Link<String> btnAgregarCiudad = new Link<String>("agregarciudad") {
			private static final long serialVersionUID = 5877529497350093033L;

			@Override
			public void onClick() {
				this.setResponsePage(new AgregarCiudadForm());
			}
		};
		this.add(btnAgregarCiudad);
		
		if (this.ctrl.hayPaisSeleccionado()) {
			this.add(new Label("filtro", ", filtro: país = " + this.ctrl.getPais().getNombre()));
			btnVerTodas.setVisible(true);
		} else {
			this.add(new Label("filtro", ""));
			btnVerTodas.setVisible(false);
		}
		
	}
	
}
