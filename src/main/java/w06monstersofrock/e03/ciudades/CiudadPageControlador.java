package w06monstersofrock.e03.ciudades;

import java.io.Serializable;
import java.util.Collection;

import p05_monsters_of_rock.modelo.Ciudad;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.stores.CiudadesStore;
import p05_monsters_of_rock.stores.PaisesStore;

public class CiudadPageControlador implements Serializable {

	private static final long serialVersionUID = -4137046215761675421L;

	private CiudadesStore cs;
	private PaisesStore ps;
	
	private Pais pais;
	
	public CiudadPageControlador() {
		super();
		this.initStores();
	}

	public Pais getPais() {
		return this.pais;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public Collection<Ciudad> getCiudadesOrdenNombre() {
		if (!this.hayPaisSeleccionado()) {
			return cs.getCiudadesOrdenNombre();
		} else {
			return cs.getCiudadesEn(this.pais);
		}
	}

	private void initStores() {
		this.cs = CiudadesStore.getInstancia();
		this.cs.cargarDatosPrueba();
		
		this.ps = PaisesStore.getInstancia();
		this.ps.cargarDatosPrueba();
	}
	
	public boolean hayPaisSeleccionado() {
		return this.pais != null;
	}

}
