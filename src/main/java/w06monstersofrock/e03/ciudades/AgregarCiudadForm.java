package w06monstersofrock.e03.ciudades;

import java.io.Serializable;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.Ciudad;

public class AgregarCiudadForm extends WebPage implements Serializable {

	private static final long serialVersionUID = -2272316496231802887L;
	
	private AgregarCiudadControlador ctrl;
	
	public AgregarCiudadForm() {
		super();
		
		this.ctrl = new AgregarCiudadControlador();
		
		this.add(this.buildForm());
	}

	private Form<Ciudad> buildForm() {
		Form<Ciudad> fc = new Form<Ciudad>("formcagregarciudad") {
			private static final long serialVersionUID = 7814773381872621896L;

			@Override
			protected void onSubmit() {
				AgregarCiudadForm.this.ctrl.agregarCiudad();
				this.setResponsePage(CiudadPage.class);
			}
			
		};
		
		fc.add(new TextField<>("ciudad", new PropertyModel<>(this.ctrl, "nombreCiudad")));
		
//		fc.add(new TextField<>("pais", new PropertyModel<>(this.ctrl, "pais")));
		fc.add(new DropDownChoice<>("pais", new PropertyModel<>(this.ctrl, "pais"),
												new PropertyModel<>(this.ctrl, "paises"),
												new ChoiceRenderer<>("nombre")));
				
		return fc;
	}
	
}
