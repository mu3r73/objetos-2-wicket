package w06monstersofrock.e03.ciudades;

import java.io.Serializable;
import java.util.Collection;

import p05_monsters_of_rock.modelo.Ciudad;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.stores.CiudadesStore;
import p05_monsters_of_rock.stores.PaisesStore;

public class AgregarCiudadControlador implements Serializable {

	private static final long serialVersionUID = -3831868509727389254L;
	
	private CiudadesStore cs;
	private PaisesStore ps;
	
	private String nombreCiudad;
	private Pais pais;
	
	public AgregarCiudadControlador() {
		super();
		this.initStores();
	}
	
	// getters / setters
	
	public String getNombreCiudad() {
		return this.nombreCiudad;
	}
	
	public void setNombreCiudad(String nombre) {
		this.nombreCiudad = nombre;
	}
	
	public Pais getPais() {
		return this.pais;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	// acciones
	
	public void agregarCiudad() {
		if (this.nombreCiudad != null) {
			Ciudad c = new Ciudad(this.nombreCiudad, this.pais);
			this.cs.agregar(c);
		}
	}
	
	private void initStores() {
		this.ps = PaisesStore.getInstancia();
		this.ps.cargarDatosPrueba();
		
		this.cs = CiudadesStore.getInstancia();
		this.cs.cargarDatosPrueba();
	}
	
	// consultas
	
	public Collection<Pais> getPaises() {
		return ps.getPaisesOrdenNombre();
	}
	
}
