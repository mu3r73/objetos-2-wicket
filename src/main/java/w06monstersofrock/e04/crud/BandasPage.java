package w06monstersofrock.e04.crud;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import p05_monsters_of_rock.modelo.Banda;
import w06monstersofrock.MoRStart;

public class BandasPage extends WebPage {

	private static final long serialVersionUID = 3041078026225967327L;
	
	private BandasPageControlador ctrl;
	
	public BandasPage() {
		super();
		
		this.ctrl = new BandasPageControlador();
		
		this.configurarPanel();
	}

	private void configurarPanel() {
		this.add(new Link<Object>("mor") {
			private static final long serialVersionUID = -8303168634756090480L;

			@Override
			public void onClick() {
				super.setResponsePage(MoRStart.class);
			}
		});
		
		ListView<Banda> lvb =
				new ListView<Banda>("bandas", new PropertyModel<>(this.ctrl, "bandasOrdenNombre")) {
					private static final long serialVersionUID = 7308013731198893545L;

					@Override
					protected void populateItem(ListItem<Banda> item) {
						Banda b = item.getModelObject();
						item.add(new Label("nombre", b.getNombre()));
						item.add(new Label("genero", b.getGenero()));
						item.add(new Label("pais", b.getPais()));
						item.add(new Label("cachet", b.getCachet()));
						
						Link<String> btnModificarBanda = new Link<String>("modificarbanda") {
							private static final long serialVersionUID = -5515885907806050663L;

							@Override
							public void onClick() {
								// TODO
							}
						
						};
						item.add(btnModificarBanda);
					}
		};
		this.add(lvb);
		
		Link<String> btnAgregarBanda = new Link<String>("agregarbanda") {
			private static final long serialVersionUID = -1654515522738748539L;

			@Override
			public void onClick() {
				// TODO
			}
		};
		this.add(btnAgregarBanda);
		
	}
	
}
