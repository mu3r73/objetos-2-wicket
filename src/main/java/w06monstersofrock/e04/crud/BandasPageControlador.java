package w06monstersofrock.e04.crud;

import java.io.Serializable;
import java.util.List;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.stores.BandasStore;

public class BandasPageControlador implements Serializable {

	private static final long serialVersionUID = 1925643092951064118L;

	private BandasStore bs;
	
	public BandasPageControlador() {
		super();
		this.initStores();
	}

	public List<Banda> getBandasOrdenNombre() {
		return bs.getBandasOrdenNombre();
	}

	private void initStores() {
		this.bs = BandasStore.getInstancia();
		this.bs.cargarDatosPrueba();
	}

}
