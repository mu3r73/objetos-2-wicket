package w00demo;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class HomePageOrig extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePageOrig() {
		add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
    }
}
