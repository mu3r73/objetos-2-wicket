package w01homepage;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

@SuppressWarnings("serial")
public class HomePage extends WebPage {

	public HomePage() {
		super();
		super.add(new Label("msg", "hola"));
	}
	
}
