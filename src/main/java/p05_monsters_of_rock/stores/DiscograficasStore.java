package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import p05_monsters_of_rock.modelo.Discografica;

public class DiscograficasStore {

	private static DiscograficasStore instancia;
	private Collection<Discografica> discograficas; 
	
	// constructores
	
	private DiscograficasStore() {
		super();
	}
	
	// instancia singleton
	
	public static DiscograficasStore getInstancia() {
		if (instancia == null) {
			instancia = new DiscograficasStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.discograficas == null) {
			Discografica olympic = new Discografica("Olympic Recordings");
			Discografica nuclearBlast = new Discografica("Nuclear Blast");
			Discografica seasonOfMist = new Discografica("Season of Mist");
			Discografica subPop = new Discografica("Sub Pop Records");
			
			this.discograficas = new HashSet<>(Arrays.asList(nuclearBlast, olympic, seasonOfMist,
					subPop));
		}
	}
	
	// consultas
	
	public Discografica getDiscografica(String nombre) {
		return this.discograficas.stream()
				.filter(discografica -> discografica.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
}
