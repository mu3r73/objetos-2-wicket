package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import p05_monsters_of_rock.modelo.Anfiteatro;
import p05_monsters_of_rock.modelo.Estadio;
import p05_monsters_of_rock.modelo.Sede;

public class SedesStore {

	private static SedesStore instancia;
	private Collection<Sede> sedes;
	
	// constructores
	
	private SedesStore() {
		super();
	}
	
	// instancia singleton
	
	public static SedesStore getInstancia() {
		if (instancia == null) {
			instancia = new SedesStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.sedes == null) {
			CiudadesStore cs = CiudadesStore.getInstancia();
			cs.cargarDatosPrueba();
			
			Sede cab = new Estadio("El Gigante de Alberdi", cs.getCiudad("Córdoba"), 22000);
			Estadio nob = new Estadio("Estadio Marcelo Bielsa", cs.getCiudad("Rosario"), 42000);
			Anfiteatro bosque = new Anfiteatro("Anfiteatro Martín Fierro", cs.getCiudad("La Plata"), 2600);
			Sede parque = new Anfiteatro("Anfiteatro Eva Perón", cs.getCiudad("Buenos Aires"), 2000);
			Sede molson = new Anfiteatro("Budweiser Stage", cs.getCiudad("Toronto"), 16000);
			Sede mosaic = new Estadio("Mosaic Stadium", cs.getCiudad("Regina"), 33000);
			Sede olympique = new Estadio("Stade Olympique", cs.getCiudad("Montreal"), 56000);
			
			this.sedes = new HashSet<>(Arrays.asList(cab, nob, bosque, parque, molson, mosaic, olympique));
		}
	}
	
	// consultas
	
	public Sede getSede(String nombre) {
		return this.sedes.stream()
				.filter(sede -> sede.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
}
