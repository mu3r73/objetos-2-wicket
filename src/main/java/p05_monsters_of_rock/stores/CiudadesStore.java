package p05_monsters_of_rock.stores;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.stream.Collectors;

import p05_monsters_of_rock.modelo.Ciudad;
import p05_monsters_of_rock.modelo.Pais;

public class CiudadesStore implements Serializable {

	private static final long serialVersionUID = -1463447469326524577L;
	
	private static CiudadesStore instancia;
	private Collection<Ciudad> ciudades;
	
	// constructores
	
	private CiudadesStore() {
		super();
	}
	
	// instancia singleton
	
	public static CiudadesStore getInstancia() {
		if (instancia == null) {
			instancia = new CiudadesStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.ciudades == null) {
			PaisesStore ps = PaisesStore.getInstancia();
			ps.cargarDatosPrueba();
			
			Ciudad bsas = new Ciudad("Buenos Aires", ps.getPais("Argentina"));
			Ciudad cordoba = new Ciudad("Córdoba", ps.getPais("Argentina"));
			Ciudad laPlata = new Ciudad("La Plata", ps.getPais("Argentina"));
			Ciudad rosario = new Ciudad("Rosario", ps.getPais("Argentina"));
			Ciudad montreal = new Ciudad("Montreal", ps.getPais("Canadá"));
			Ciudad regina = new Ciudad("Regina", ps.getPais("Canadá"));
			Ciudad toronto = new Ciudad("Toronto", ps.getPais("Canadá"));
			Ciudad vancouver = new Ciudad("Vancouver", ps.getPais("Canadá"));
			
			this.ciudades = new HashSet<>(Arrays.asList(bsas, cordoba, laPlata, rosario,
					montreal, regina, toronto, vancouver));
		}
	}
	
	// consultas
	
	public Collection<Ciudad> getCiudadesOrdenNombre() {
		return this.ciudades.stream()
				.sorted(Comparator.comparing(ciudad -> ciudad.getNombre()))
				.collect(Collectors.toList());
	
	}
	
	public Ciudad getCiudad(String nombre) {
		return this.ciudades.stream()
				.filter(ciudad -> ciudad.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
	public Collection<Ciudad> getCiudadesEn(Pais pais) {
		return this.ciudades.stream()
				.filter(ciudad -> ciudad.getPais().equals(pais))
				.sorted(Comparator.comparing(ciudad -> ciudad.getNombre()))
				.collect(Collectors.toList());
	}

	public void agregar(Ciudad ciudad) {
		this.ciudades.add(ciudad);		
	}
	
}
