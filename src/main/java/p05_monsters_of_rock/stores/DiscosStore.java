package p05_monsters_of_rock.stores;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Pais;

public class DiscosStore implements Serializable {

	private static final long serialVersionUID = 2716613485204239977L;
	
	private static DiscosStore instancia;
	private Collection<Disco> discos;
	
	private PaisesStore ps;
	
	// constructores
	
	private DiscosStore() {
		super();
	}
	
	// instancia singleton
	
	public static DiscosStore getInstancia() {
		if (instancia == null) {
			instancia = new DiscosStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.discos == null) {
			this.ps = PaisesStore.getInstancia();
			this.ps.cargarDatosPrueba();
			
			DiscograficasStore ds = DiscograficasStore.getInstancia();
			ds.cargarDatosPrueba();
			
			BandasStore bs = BandasStore.getInstancia();
			bs.cargarDatosPrueba();
			
			Disco concealed = new Disco("Concealed", bs.getBanda("Augury"), 2004, ds.getDiscografica("Season of Mist"));
			Disco fragmentaryEvidence = new Disco("Fragmentary Evidence", bs.getBanda("Augury"), 2009, ds.getDiscografica("Season of Mist"));
			
			Disco leprosy = new Disco("Leprosy", bs.getBanda("Death"), 1988, ds.getDiscografica("Nuclear Blast"));
			Disco spiritualHealing = new Disco("Spiritual Healing", bs.getBanda("Death"), 1990, ds.getDiscografica("Nuclear Blast"));
			Disco human = new Disco("Human", bs.getBanda("Death"), 1991, ds.getDiscografica("Nuclear Blast"));
			Disco symbolic = new Disco("Symbolic", bs.getBanda("Death"), 1995, ds.getDiscografica("Nuclear Blast"));
			
			Disco consideredDead = new Disco("Considered Dead", bs.getBanda("Gorguts"), 1991, ds.getDiscografica("Nuclear Blast"));
			Disco erosionOfSanity = new Disco("The Erosion of Sanity", bs.getBanda("Gorguts"), 1993, ds.getDiscografica("Nuclear Blast"));
			Disco obscura = new Disco("Obscura", bs.getBanda("Gorguts"), 1998, ds.getDiscografica("Olympic Recordings"));
			Disco wisdom2hate = new Disco("From Wisdom to Hate", bs.getBanda("Gorguts"), 2001, ds.getDiscografica("Olympic Recordings"));
			Disco coloredSands = new Disco("Colored Sands", bs.getBanda("Gorguts"), 2013, ds.getDiscografica("Season of Mist"));
			
			Disco syzygialMiscreancy = new Disco("Syzygial Miscreancy", bs.getBanda("Hellwitch"), 1990, ds.getDiscografica("Season of Mist"));
			Disco omnipotentConvocation = new Disco("Omnipotent Convocation", bs.getBanda("Hellwitch"), 2009, ds.getDiscografica("Season of Mist"));
			
			Disco hopelessHopes = new Disco("Hopeless Hopes", bs.getBanda("Martyr"), 1997, ds.getDiscografica("Season of Mist"));
			Disco warpZone = new Disco("Warp Zone", bs.getBanda("Martyr"), 2000, ds.getDiscografica("Season of Mist"));
			Disco feedingTheAbscess = new Disco("Feeding the Abscess", bs.getBanda("Martyr"), 2006, ds.getDiscografica("Season of Mist"));
			
			Disco paleDebilitatingAutumn = new Disco("A Pale Debilitating Autumn", bs.getBanda("Pavor"), 1994, ds.getDiscografica("Nuclear Blast"));
			Disco furioso = new Disco("Furioso", bs.getBanda("Pavor"), 2003, ds.getDiscografica("Nuclear Blast"));
			
			Disco arise = new Disco("Arise", bs.getBanda("Sepultura"), 1991, ds.getDiscografica("Nuclear Blast"));
			Disco chaosAD = new Disco("Chaos A.D.", bs.getBanda("Sepultura"), 1993, ds.getDiscografica("Nuclear Blast"));
			Disco roots = new Disco("Roots", bs.getBanda("Sepultura"), 1996, ds.getDiscografica("Nuclear Blast"));
			Disco against = new Disco("Against", bs.getBanda("Sepultura"), 1998, ds.getDiscografica("Season of Mist"));
			
			Disco hellAwaits = new Disco("Hell Awaits", bs.getBanda("Slayer"), 1985, ds.getDiscografica("Nuclear Blast"));
			Disco reignInBlood = new Disco("Reign in Blood", bs.getBanda("Slayer"), 1986, ds.getDiscografica("Nuclear Blast"));
			Disco southOfHeaven = new Disco("South of Heaven", bs.getBanda("Slayer"), 1988, ds.getDiscografica("Nuclear Blast"));
			
			Disco redAlbum = new Disco("Red Album", bs.getBanda("Baroness"), 2007, ds.getDiscografica("Nuclear Blast"));
			Disco blueRecord = new Disco("Blue Record", bs.getBanda("Baroness"), 2009, ds.getDiscografica("Nuclear Blast"));
			
			Disco focus = new Disco("Focus", bs.getBanda("Cynic"), 1993, ds.getDiscografica("Nuclear Blast"));
			Disco tracedInAir = new Disco("Traced in Air", bs.getBanda("Cynic"), 2008, ds.getDiscografica("Season of Mist"));
			Disco kindlyBent = new Disco("Kindly Bent to Free Us", bs.getBanda("Cynic"), 2014, ds.getDiscografica("Nuclear Blast"));
			
			Disco imagesNWords = new Disco("Images and Words", bs.getBanda("Dream Theater"), 1992, ds.getDiscografica("Nuclear Blast"));
			Disco awake = new Disco("Awake", bs.getBanda("Dream Theater"), 1994, ds.getDiscografica("Nuclear Blast"));
			Disco fallingIntoInf = new Disco("Falling into Infinity", bs.getBanda("Dream Theater"), 1997, ds.getDiscografica("Nuclear Blast"));
			Disco metroPt2 = new Disco("Metropolis Pt. 2: Scenes from a Memory", bs.getBanda("Dream Theater"), 1999, ds.getDiscografica("Nuclear Blast"));
			Disco sixDegrees = new Disco("Six Degrees of Inner Turbulence", bs.getBanda("Dream Theater"), 2002, ds.getDiscografica("Nuclear Blast"));
			Disco trainOfThought = new Disco("Train of Thought", bs.getBanda("Dream Theater"), 2003, ds.getDiscografica("Nuclear Blast"));
			Disco octavarium = new Disco("Octavarium", bs.getBanda("Dream Theater"), 2005, ds.getDiscografica("Nuclear Blast"));
			Disco systematicChaos = new Disco("Systematic Chaos", bs.getBanda("Dream Theater"), 2007, ds.getDiscografica("Nuclear Blast"));
			
			Disco remission = new Disco("Remission", bs.getBanda("Mastodon"), 2002, ds.getDiscografica("Season of Mist"));
			Disco leviathan = new Disco("Leviathan", bs.getBanda("Mastodon"), 2004, ds.getDiscografica("Season of Mist"));
			Disco bloodMountain = new Disco("Blood Mountain", bs.getBanda("Mastodon"), 2006, ds.getDiscografica("Season of Mist"));
			Disco crackTheSkye = new Disco("Crack the Skye", bs.getBanda("Mastodon"), 2009, ds.getDiscografica("Season of Mist"));
			Disco hunter = new Disco("The Hunter", bs.getBanda("Mastodon"), 2011, ds.getDiscografica("Season of Mist"));
			
			Disco damnationGame = new Disco("The Damnation Game", bs.getBanda("Symphony X"), 1995, ds.getDiscografica("Nuclear Blast"));
			Disco divineWings = new Disco("The Divine Wings of Tragedy", bs.getBanda("Symphony X"), 1997, ds.getDiscografica("Nuclear Blast"));
			Disco twilightInOlympus = new Disco("Twilight in Olympus", bs.getBanda("Symphony X"), 1998, ds.getDiscografica("Nuclear Blast"));
			Disco newMythologySuite = new Disco("V: The New Mythology Suite", bs.getBanda("Symphony X"), 2000, ds.getDiscografica("Nuclear Blast"));
			Disco oddysey = new Disco("The Oddysey", bs.getBanda("Symphony X"), 2002, ds.getDiscografica("Nuclear Blast"));
			Disco paradiseLost = new Disco("Paradise Lost", bs.getBanda("Symphony X"), 2007, ds.getDiscografica("Nuclear Blast"));
			Disco iconoclast = new Disco("Iconoclast", bs.getBanda("Symphony X"), 2011, ds.getDiscografica("Nuclear Blast"));
			Disco underworld = new Disco("Underworld", bs.getBanda("Symphony X"), 2015, ds.getDiscografica("Nuclear Blast"));
			
			Disco polars = new Disco("Polars", bs.getBanda("Textures"), 2003, ds.getDiscografica("Nuclear Blast"));
			Disco drawingCircles = new Disco("Drawing Circles", bs.getBanda("Textures"), 2006, ds.getDiscografica("Nuclear Blast"));
			
			Disco undertow = new Disco("Undertow", bs.getBanda("Tool"), 1993, ds.getDiscografica("Nuclear Blast"));
			Disco aenima = new Disco("Ænima", bs.getBanda("Tool"), 1996, ds.getDiscografica("Nuclear Blast"));
			Disco lateralus = new Disco("Lateralus", bs.getBanda("Tool"), 2001, ds.getDiscografica("Nuclear Blast"));
			Disco tenKdays = new Disco("10,000 Days", bs.getBanda("Tool"), 2006, ds.getDiscografica("Nuclear Blast"));
			
			Disco one = new Disco("One", bs.getBanda("Tesseract"), 2010, ds.getDiscografica("Season of Mist"));
			
			Disco facelift = new Disco("Facelift", bs.getBanda("Alice in Chains"), 1990, ds.getDiscografica("Sub Pop Records"));
			Disco dirt = new Disco("Dirt", bs.getBanda("Alice in Chains"), 1992, ds.getDiscografica("Sub Pop Records"));
			Disco aliceInChains = new Disco("Alice In Chains", bs.getBanda("Alice in Chains"), 1995, ds.getDiscografica("Sub Pop Records"));
			
			Disco bleach = new Disco("Bleach", bs.getBanda("Nirvana"), 1989, ds.getDiscografica("Sub Pop Records"));
			Disco nevermind = new Disco("Nevermind", bs.getBanda("Nirvana"), 1991, ds.getDiscografica("Sub Pop Records"));
			Disco incesticide = new Disco("Incesticide", bs.getBanda("Nirvana"), 1992, ds.getDiscografica("Sub Pop Records"));
			Disco inUtero = new Disco("In Utero", bs.getBanda("Nirvana"), 1993, ds.getDiscografica("Sub Pop Records"));
			
			Disco ten = new Disco("Ten", bs.getBanda("Pearl Jam"), 1991, ds.getDiscografica("Sub Pop Records"));
			Disco vs = new Disco("Vs", bs.getBanda("Pearl Jam"), 1993, ds.getDiscografica("Sub Pop Records"));
			Disco vitalogy = new Disco("Vitalogy", bs.getBanda("Pearl Jam"), 1994, ds.getDiscografica("Sub Pop Records"));
			Disco noCode = new Disco("No Code", bs.getBanda("Pearl Jam"), 1996, ds.getDiscografica("Sub Pop Records"));
			Disco yield = new Disco("Yield", bs.getBanda("Pearl Jam"), 1998, ds.getDiscografica("Sub Pop Records"));
			
			Disco ultramegaOK = new Disco("Ultramega OK", bs.getBanda("Soundgarden"), 1988, ds.getDiscografica("Sub Pop Records"));
			Disco louderThanLove = new Disco("Louder Than Love", bs.getBanda("Soundgarden"), 1989, ds.getDiscografica("Sub Pop Records"));
			Disco badmotorfinger = new Disco("Badmotorfinger", bs.getBanda("Soundgarden"), 1991, ds.getDiscografica("Sub Pop Records"));
			Disco superunknown = new Disco("Superunknown", bs.getBanda("Soundgarden"), 1994, ds.getDiscografica("Sub Pop Records"));
			Disco downOnTheUpside = new Disco("Down on the Uthis.pside", bs.getBanda("Soundgarden"), 1996, ds.getDiscografica("Sub Pop Records"));
			
			Disco core = new Disco("Core", bs.getBanda("Stone Temple Pilots"), 1992, ds.getDiscografica("Sub Pop Records"));
			
			this.discos = new HashSet<>(Arrays.asList(concealed, fragmentaryEvidence, leprosy,
					spiritualHealing, human, symbolic, consideredDead, erosionOfSanity, obscura,
					wisdom2hate, coloredSands, syzygialMiscreancy, omnipotentConvocation, hopelessHopes,
					warpZone, feedingTheAbscess, paleDebilitatingAutumn, furioso, arise, chaosAD, roots,
					against, hellAwaits, reignInBlood, southOfHeaven, redAlbum, blueRecord, focus,
					tracedInAir, kindlyBent, imagesNWords, awake, fallingIntoInf, metroPt2, sixDegrees,
					trainOfThought, octavarium, systematicChaos, remission, leviathan, bloodMountain,
					crackTheSkye, hunter, damnationGame, divineWings, twilightInOlympus, newMythologySuite,
					oddysey, paradiseLost, iconoclast, underworld, polars, drawingCircles, undertow,
					aenima, lateralus, tenKdays, one, facelift, dirt, aliceInChains, bleach, nevermind,
					incesticide, inUtero, ten, vs, vitalogy, noCode, yield, ultramegaOK, louderThanLove,
					badmotorfinger, superunknown, downOnTheUpside, core));
			
//			obscura.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
//			reignInBlood.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
//			tracedInAir.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
//			superunknown.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
//			nevermind.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);

			this.randomizarCopiasVendidas();
			
		}
	}
	
	private void randomizarCopiasVendidas() {
		for (Disco d : this.discos) {
			for (Pais p : this.ps.getPaisesOrdenNombre()) {
				double chance = Math.random();
				if (chance > .5) {
					// hay copias vendidas a partir de random(0, 1) > 0.5
					// la cantidad de copias vendidas depende del mismo random(0, 1)
					// conversión lineal a 500 .. 10000
					d.agregarCopiasVendidas(p, (int)(500 + 19000 * (chance - .5)));
				}
			}
		}
	}

	// consultas
	
	public Disco getDisco(String nombre) {
		return this.discos.stream()
				.filter(disco -> disco.getNombre().equals(nombre))
				.findAny()
				.get();
	}

}
