package p05_monsters_of_rock.stores;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import p05_monsters_of_rock.modelo.Banda;

public class BandasStore implements Serializable {

	private static final long serialVersionUID = 1449894990675440737L;
	
	private static BandasStore instancia;
	private Collection<Banda> bandas;
	
	// constructores
	
	private BandasStore() {
		super();
	}
	
	// instancia singleton
	
	public static BandasStore getInstancia() {
		if (instancia == null) {
			instancia = new BandasStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.bandas == null) {
			PaisesStore ps = PaisesStore.getInstancia();
			ps.cargarDatosPrueba();
			
			Banda augury = new Banda("Augury", "metal", 50000, ps.getPais("Canadá"));
			Banda death = new Banda("Death", "metal", 150000, ps.getPais("EEUU"));
			Banda gorguts = new Banda("Gorguts", "metal", 125000, ps.getPais("Canadá"));
			Banda hellwitch =  new Banda("Hellwitch", "metal", 75000, ps.getPais("EEUU"));
			Banda martyr = new Banda("Martyr", "metal", 50000, ps.getPais("Canadá"));
			Banda pavor = new Banda("Pavor", "metal", 75000, ps.getPais("Alemania"));
			Banda sepultura = new Banda("Sepultura", "metal", 80000, ps.getPais("Brasil"));
			Banda slayer = new Banda("Slayer", "metal", 125000, ps.getPais("EEUU"));
			
			Banda baroness = new Banda("Baroness", "rock", 50000, ps.getPais("EEUU"));
			Banda cynic = new Banda("Cynic", "rock", 100000, ps.getPais("EEUU"));
			Banda dreamTheater = new Banda("Dream Theater", "rock", 120000, ps.getPais("EEUU"));
			Banda mastodon = new Banda("Mastodon", "rock", 80000, ps.getPais("EEUU"));
			Banda symphonyX = new Banda("Symphony X", "rock", 90000, ps.getPais("EEUU"));
			Banda textures = new Banda("Textures", "rock", 50000, ps.getPais("Países Bajos"));
			Banda tool = new Banda("Tool", "rock", 75000, ps.getPais("EEUU"));
			
			Banda tesseract = new Banda("Tesseract", "djent", 7500, ps.getPais("Inglaterra"));
			
			Banda aliceInChains = new Banda("Alice in Chains", "grunge", 90000, ps.getPais("EEUU"));
			Banda nirvana = new Banda("Nirvana", "grunge", 100000, ps.getPais("EEUU"));
			Banda pearlJam = new Banda("Pearl Jam", "grunge", 90000, ps.getPais("EEUU"));
			Banda soundgarden = new Banda("Soundgarden", "grunge", 100000, ps.getPais("EEUU"));
			Banda stoneTemplePilots = new Banda("Stone Temple Pilots", "grunge", 90000, ps.getPais("EEUU"));
			
			this.bandas = new HashSet<>(Arrays.asList(augury, death, gorguts, hellwitch, martyr, pavor,
					sepultura, slayer, baroness, cynic, dreamTheater, mastodon, symphonyX, textures, tool,
					tesseract, aliceInChains, nirvana, pearlJam, soundgarden, stoneTemplePilots));
		}
	}
	
	// consultas
	
	public List<Banda> getBandasOrdenNombre() {
		return this.bandas.stream()
				.sorted(Comparator.comparing(banda -> banda.getNombre()))
				.collect(Collectors.toList());
	}
	
	public Banda getBanda(String nombre) {
		return this.bandas.stream()
				.filter(banda -> banda.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
}
