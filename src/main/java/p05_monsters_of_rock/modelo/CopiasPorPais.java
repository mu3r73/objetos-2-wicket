package p05_monsters_of_rock.modelo;

import java.io.Serializable;

/**
 * copias vendidas (de un disco) en un país determinado
 */
public class CopiasPorPais implements Serializable {

	private static final long serialVersionUID = 4337872543584570321L;
	
	private Pais pais;
	private int copias;
	
	// construtores
	
	public CopiasPorPais() {
		super();
	}

	public CopiasPorPais(Pais pais, int copias) {
		super();
		this.pais = pais;
		this.copias = copias;
	}

	// getters / setters
	
	public Pais getPais() {
		return this.pais;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public int getCopias() {
		return this.copias;
	}
	
	public void setCopias(int copias) {
		this.copias = copias;
	}
	
}
