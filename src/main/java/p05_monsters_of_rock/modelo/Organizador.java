package p05_monsters_of_rock.modelo;

/**
 * organizador de eventos
 */
public interface Organizador {

	/**
	 * indica si el organizador puede trabajar en la sede 
	 */
	public void checkPuedeTrabajarEn(Sede sede);

	/**
	 * verifica si la banda puede participar (con el disco)
	 * en eventos organizados por el organizador
	 */
	public void checkPuedeTrabajarCon(Banda banda, Disco disco);

}
