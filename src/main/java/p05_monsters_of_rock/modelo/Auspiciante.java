package p05_monsters_of_rock.modelo;

/**
 * auspiciante para un festival
 * (tiene un monto aportado)
 */
public class Auspiciante {
	
	private String nombre;
	private double aporte;
	
	// constructores
	
	public Auspiciante(String nombre, double aporte) {
		super();
		this.nombre = nombre;
		this.aporte = aporte;
	}

	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public double getAporte() {
		return aporte;
	}

}
