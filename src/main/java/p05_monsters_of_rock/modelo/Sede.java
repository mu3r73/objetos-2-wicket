package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * sede genérica para eventos
 * (tiene una capacidad, y una colección de eventos)
 */
public abstract class Sede {

	private String nombre;
	private Ciudad ciudad;
	private int capacidad;
	private Set<Evento> eventos = new HashSet<>();

	// constructores
	
	public Sede(String nombre, Ciudad ciudad, int capacidad) {
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.ciudad.agregarSede(this);
		this.capacidad = capacidad;
	}

	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Ciudad getCiudad() {
		return this.ciudad;
	}
	
	public int getCapacidad() {
		return this.capacidad;
	}
	
	public Set<Evento> getEventos() {
		return this.eventos;
	}
	
	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

	// altas a colecciones
	
	/**
	 * agrega un evento a la sede
	 * usado por Evento.agregarseASedeYAdmin()
	 */
	protected void agregarEvento(Evento evento) {
		this.eventos.add(evento);
	}
	
	// consultas
	
	/**
	 * req 3:
	 * retorna las bandas que participan en eventos a desarrollarse en la sede 
	 */
	public Set<Banda> bandasQueParticipanEnEventosEnSede() {
		return this.eventos.stream()
				.flatMap(evento -> evento.todasLasBandas().stream())
				.collect(Collectors.toSet());
	}
	
	/**
	 * indica si el evento puede realizarse en la sede
	 */
	protected abstract void checkPuedeRecibir(Evento evento);

	/**
	 * retorna el costo del alquiler de la sede, para el evento
	 */
	protected abstract double costoAlquiler(Evento evento);

	/**
	 * retorna las bandas extranjeras que participan en eventos en el país
	 */
	protected Set<Banda> bandasExtranjerasQueParticipanEnEventos() {
		return this.eventos.stream()
				.flatMap(evento -> evento.todasLasBandas().stream())
				.filter(evento -> evento.getPais() != this.getPais())
				.collect(Collectors.toSet());
	}

	/**
	 * retorna el país de la sede
	 */
	public Pais getPais() {
		return this.ciudad.getPais();
	}

}
