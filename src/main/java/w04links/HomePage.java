package w04links;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class HomePage extends WebPage {
	
	private PageParameters args1 = new PageParameters();
	private PageParameters args2 = new PageParameters();
	private PageParameters args3 = new PageParameters();
	
	public HomePage() {
		super();
		
		args1.add("nombre", "Sean");
		args1.add("apellido", "Reinert");
		args2.add("nombre", "Vinnie");
		args2.add("apellido", "Paul");
		args3.add("nombre", "Dave");
		args3.add("apellido", "Grohl");
		
		super.add(new Label("lbl", "les drummers"));
		
		// link con clase anónima
		super.add(new Link<Object>("lnk1") {
			
			@Override
			public void onClick() {
				super.setResponsePage(LinkedPage.class, args1);
			}
		});
		
		// bookmark page link, sin clase anónima
		super.add(new BookmarkablePageLink<>("lnk2", LinkedPage.class, args2));
	}
	
}
