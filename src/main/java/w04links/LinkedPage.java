package w04links;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class LinkedPage extends WebPage {

	public LinkedPage(PageParameters parameters) {
		super(parameters);
		
		super.add(new Label("lblnombre", "valor de \"nombre\": " + parameters.get("nombre")));
		super.add(new Label("lblapellido", "valor de \"apellido\": " + parameters.get("apellido")));		
	}
	
}
