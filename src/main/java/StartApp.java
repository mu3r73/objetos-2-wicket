import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

public class StartApp extends WebApplication {
	
	@Override
	public Class<? extends Page> getHomePage() {
		return Start.class;
	}
	
//	@Override
//	public RuntimeConfigurationType getConfigurationType() {
//		return RuntimeConfigurationType.DEPLOYMENT;
//	}
	
}
