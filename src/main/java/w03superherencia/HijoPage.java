package w03superherencia;

import org.apache.wicket.markup.html.basic.Label;

@SuppressWarnings("serial")
public class HijoPage extends PadrePage {

	public HijoPage() {
		super();
		super.add(new Label("msg_hijo", "hola @ hijo"));
	}
	
}
