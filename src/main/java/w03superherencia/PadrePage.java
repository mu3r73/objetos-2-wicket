package w03superherencia;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

@SuppressWarnings("serial")
public class PadrePage extends WebPage {

	public PadrePage() {
		super();
		super.add(new Label("msg_padre", "hola @ padre"));
	}
	
}
