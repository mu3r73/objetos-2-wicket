package w03superherencia;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

public class HerenciaApp extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return HijoPage.class;
	}

}
