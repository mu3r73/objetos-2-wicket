package w02herencia;

import org.apache.wicket.markup.html.basic.Label;

@SuppressWarnings("serial")
public class HijoPage extends PadrePage {

	public HijoPage() {
		super();
		super.add(new Label("msg", "hola @ hijo"));
	}
	
}
